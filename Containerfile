FROM elixir:1.14.2-alpine as build

ARG BUILD_DATE

ARG MIX_ENV=prod \
    OAUTH_CONSUMER_STRATEGIES=""
    #OAUTH_CONSUMER_STRATEGIES="twitter facebook google microsoft slack github keycloak:ueberauth_keycloak_strategy"

WORKDIR /src

RUN echo "http://nl.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories 
RUN apk update &&\
    apk add git bash gcc g++ musl-dev erlang elixir make cmake file-dev 
RUN git clone https://gitlab.com/soapbox-pub/rebased.git /src
RUN mix local.hex --force &&\
    mix local.rebar --force &&\
    mix deps.get --only prod &&\
    mkdir release &&\
    mix release --path release


RUN git clone https://github.com/facebookresearch/fastText.git ./fasttext &&\
    cd fasttext && make


FROM alpine:3.16

ARG BUILD_DATE
ARG VCS_REF

ENV TZ="Etc/UTC"

LABEL maintainer="hello@soapbox.pub" \
    org.opencontainers.image.title="rebased" \
    org.opencontainers.image.description="Rebased" \
    org.opencontainers.image.authors="hello@soapbox.pub" \
    org.opencontainers.image.vendor="soapbox.pub" \
    org.opencontainers.image.documentation="https://gitlab.com/soapbox-pub/rebased" \
    org.opencontainers.image.licenses="AGPL-3.0" \
    org.opencontainers.image.url="https://soapbox.pub" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE


ARG HOME=/opt/pleroma
ARG DATA=/var/lib/pleroma

RUN apk update &&\
    apk add curl ca-certificates imagemagick libmagic ffmpeg postgresql-client elixir bash exiftool

RUN addgroup rebased &&\
    adduser --system --shell /bin/false -G rebased --home ${HOME} rebased &&\
    mkdir -p ${DATA}/uploads &&\
    mkdir -p ${DATA}/static &&\
    chown -R rebased ${DATA} &&\
    mkdir -p /etc/pleroma &&\
    chown -R rebased /etc/pleroma 

RUN mkdir -p /usr/share/fasttext &&\
    curl -L https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.ftz -o /usr/share/fasttext/lid.176.ftz &&\
    chmod 0644 /usr/share/fasttext/lid.176.ftz

COPY --from=build --chown=rebased:rebased /src/release ${HOME}
COPY --from=build --chown=rebased:rebased /src/config/docker.exs /etc/pleroma/config.exs 
COPY --from=build --chown=rebased:rebased /src/docker-entrypoint.sh ${HOME}
COPY --from=build /src/fasttext/fasttext /usr/bin/fasttext



RUN chown -R rebased ${HOME} ${DATA}
RUN chmod +x ${HOME}/docker-entrypoint.sh

EXPOSE 5000

WORKDIR /var/lib/pleroma 

USER rebased

ENTRYPOINT ["/opt/pleroma/docker-entrypoint.sh"]
