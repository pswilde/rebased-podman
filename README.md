# THIS IS NOW OBSOLETE
Please use https://codeberg.org/pswilde/pleroma-forks-podman instead

# Rebased (Soapbox BE)

[Rebased](https://soapbox.pub/2022/08/19/soapbox-be-is-now-rebased/) is a federated social networking
platform, compatible with Mastodon, Pleroma, GNU Social, and other ActivityPub implementations. It is free software licensed uner the AGPLv3

It actually consists of two components: a backend, named Rebased, and a user-facing frontend, named Soapbox (previously Soapbox FE).

Its main advantages are its lightness and speed.

![Rebased](https://media.soapbox.pub/uploads/2022/08/signal-2022-08-18-153539_002.png)

## Features

This podman set up build and creates a Rebased container image using Alpine Linux.  
Working through each script will allow you to set up your own Rebased server.  
  
For a more "official" set up, please use the `ubuntu` branch of this repo which builds the image directly using Rebased's own Dockerfile.

## Build-time variables

Variables are entered into `script_config.sh` - (copy `script_config_sample.sh`), 
the key variables being `PG_USER`, `PG_PASS`, `PG_HOST`, `PG_NAME` if you have a database running elsewhere

### Installation

Each script is numbered which give the order in which they need to be run - scripts with `5` in the name should only need to be run once.  
 1. `00-create-pod.sh` - this creates a simple pod, in which the other containers will exist. It will also open port 5000 for incoming web access and map the folders in which persistent data will reside.
 2. `10-build-rebased.sh` - this will clone the current develop git repo and build an image from it. Re-running this script will effectively update the version of Rebased. This may take 10 minutes or so to run as it compiles the software.
 3. `20-create-db.sh` - this will create a postgres container in which the database will exist
 4. `30-run-rebased.sh` - this creates the main Rebased container and starts the web server on port 5000 (you'll need a reverse proxy (HAProxy, httpd, nginx, etc.) to publish it). At start up, this will check the database and create the appropriate tables and extensions so things will work.
 5. `35-gen-config.sh` - YOU SHOULD ONLY NEED TO RUN THIS ONCE. Will generate a `generated_config.exs` file. Make sure you set the port number correctly (5000, not 4000). Once generated, it should be renamed to `prod.secret.exs` to make it available at run-time
 6. at this point you should re-run `30-run-rebased.sh`
 7. Now, you're instance should be running on your device at port 5000 - test it and find out.
 8. `40-install-soapbox.sh` - this installs the [Soapbox](https://soapbox.pub/) front end - which is where you will log in and ultimately use the system.

Once the above steps are complete, you're nearly ready to go! The final steps are to 
 1. Create your admin user
 2. Configure your reverse proxy

### Create your admin user
You'll need to create an admin user so you can log in and configure more users and settings.  
Simply run  
```sh
podman exec rebased-web /opt/pleroma/bin/pleroma_ctl user new <username> <email@address.com> --admin
```
This will generate the user, set it up as admin, and provide a password reset link. Go to this password reset link to set the new password and you're done!  

#### Configure your reverse proxy
This will differ from device to device - but check some of the configs in the [Rebased GitLab Repo](https://gitlab.com/soapbox-pub/rebased/-/tree/develop/installation) for further information.  

### Congratulations!
If all has gone well, you should be done. Well done. Now get out there and talk to some people!

## Final Notes

As with anything, I've only tested this on my systems, I have tried my best to re-run these scripts to ensure all events are covered, but I may have missed some.  
If you do have troubles, either create an issue here, or contact me on the Fediverse at
[@paul@notnull.click](https://notnull.click/@paul) and I'll try to help.

