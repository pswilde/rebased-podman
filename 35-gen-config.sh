#!/bin/bash

source ./script_config.sh

podman exec -it --user=0 $POS_NAME-web /opt/pleroma/bin/pleroma_ctl instance gen
podman exec $POD_NAME-web cat /src/config/generated_config.exs > ./config/prod.secret.exs
echo .
echo =========================
echo "Check ./config/prod.secret.exs and if all looks OK, restart rebased-web container"


