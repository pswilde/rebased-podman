#!/bin/bash

source ./script_config.sh
touch ./config/prod.secret.exs

echo Stopping old 'rebased-web' container...
podman stop $POD_NAME-web
podman rm $POD_NAME-web
chmod -R 777 uploads static

echo Creating new 'rebased-web' container...
podman run -d \
    --name $POD_NAME-web \
    --pod $POD_NAME-pod \
    -e DB_USER=$PG_USER \
    -e DB_PASS=$PG_PASS \
    -e DB_NAME=$PG_NAME \
    -e DB_HOST=$PG_HOST \
    -v ./uploads:/var/lib/pleroma/uploads:z \
    -v ./static:/var/lib/pleroma/static:z \
    -v ./config:/var/lib/pleroma/config \
    -v ./config/prod.secret.exs:/var/lib/pleroma/config.exs \
    $REBASED_IMG


