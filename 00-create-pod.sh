#!/bin/bash

mkdir ./uploads ./static ./config -p

if [ ! -f ./script_config.sh ]; then
	cp ./script_config_sample.sh ./script_config.sh
fi
source ./script_config.sh
podman pod create \
    --name $POD_NAME-pod \
    -p $PORT:5000
