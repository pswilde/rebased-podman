#!/bin/bash

DATE=$(date +"%Y-%m-%d")
PORT=5000
POD_NAME=rebased
PG_USER=rebased
PG_PASS=rebased
PG_HOST=localhost
PG_NAME=rebased
REBASED_VER=develop
REBASED_IMG="rebased-$REBASED_VER-$DATE"
if [ -f ./.last ]; then
	REBASED_IMG=$(cat ./.last)
fi

echo $REBASED_IMG
